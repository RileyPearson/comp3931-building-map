class Graph:
    nodes = []
    edges = []

    def __init__(self, n, e):                                                   # INIT
        self.nodes = n
        self.edges = e


    def printNodes(self):                                                       # Print graph
        print(self.nodes)
        print(self.edges)


    def getEdges(self, ID):                                                     # Get all edges connected to a node
        l = []
        for i in self.edges:
            if i[0] == ID or i[1] == ID:
                l.append(i)

        return l


    def getAccEdges(self, ID):                                                  # Get all Accessible edges connected to a node
        l = []
        for i in self.edges:
            if i[0] == ID or i[1] == ID:
                if i[3] == 1:
                    l.append(i)

        return l


    def getNodes(self):                                                         # Get all nodes in graph
        return self.nodes


    def prune(self, node, dest):                                                # Check if node is an unrelated room
        if node != dest:
            i = self.nodes[0]
            for x in self.nodes:
                if x[0] == node:
                    i = x
            if i[2] == 1:
                return 0
        return 1
