from algo import graphDijkstra
import math

class Dijkstra:
    nodes = []

    def __init__(self, gNodes):                                                 # INIT
        curr = []

        for i in gNodes:
            curr =  (i[0], 0, 0, 0, 0)
            self.nodes.append(list(curr))


    def getNode(self, ID):                                                      # Get node by ID
        j = self.nodes[0]
        change = 0
        for i in self.nodes:
            if i[0] == ID:
                j = i
                change = 1

        if change == 0:             # check if a node was actually found
            raise AssertionError('ERROR: A node could not be found! (' + str(ID) + ')')

        return j


    def setNode(self, ID, vis, dis, prev, val):                                 # Set node by ID
        for i in range(len(self.nodes)):
            if self.nodes[i][0] == ID:
                self.nodes[i][1] = vis
                self.nodes[i][2] = dis
                self.nodes[i][3] = prev
                self.nodes[i][4] = val


    def visit(self, graph, acc, end):                                           # Visit
        min = (0, math.inf)         # determine which node to visit
        for i in self.nodes:
            if i[1] == 0 and i[4] == 1:
                if i[2] < min[1]:
                    min = (i[0], i[2])
        #print(min[0], end = " ")

        if min[1] == math.inf:      # if all connected nodes visited but
             raise AssertionError('ERROR: No path could be found!') # no path found

        focus = self.getNode(min[0])

        if acc == 0:                # if acc == 1 only use accessible paths
            edges = graph.getEdges(min[0])
        else:
            edges = graph.getAccEdges(min[0])
        #print(edges)
                                    # mark chosen node as visited
        self.setNode(focus[0], 1, focus[2], focus[3], focus[4])

        for edge in edges:          # update shortest paths for connected nodes
            if edge[0] == focus[0]:
                i = self.getNode(edge[1])
                if i[4] == 0 or i[2] > (focus[2]+edge[2]):

                    if graph.prune(edge[1], end):   # exclude unrelated rooms
                        self.setNode(edge[1], 0, focus[2]+edge[2], focus[0], 1)
            else:
                i = self.getNode(edge[0])
                if i[4] == 0 or i[2] > (focus[2]+edge[2]):

                    if graph.prune(edge[0], end):
                        self.setNode(edge[0], 0, focus[2]+edge[2], focus[0], 1)


    def solve(self, graph, start, end, acc):                                    # Solve
        for i in self.nodes:        # initialize sorting array
            if i[0] == start:
                self.setNode(i[0], 0, 0, i[0], 1)
            else:
                self.setNode(i[0], 0, 0, 0, 0)


        while(self.getNode(end)[1] != 1):    # while destination not visited
            self.visit(graph, acc, end)


        #    print(str(self.nodes))     # use this to view dijkstra iterations
        #    print("-")

        path = []                       # generate path from solved array
        i = self.getNode(end)
        while(i[0] != i[3]):
            path.append(i[0])
            i = self.getNode(i[3])

        path.append(start)
        path.reverse()

        return path
