from algo import graphDijkstra
from algo import sortDijkstra

from sys import argv


# initialize user input
filename, a, b, c = argv

start = int(a)
dest = int(b)
acc = int(c)




# initialize graph lists from csv files

with open("data/nodes.csv") as f:    # construct node list
    content = f.readlines()

n = []
for i in content:
    x = i.split(',')
    x[0] = int(x[0])
    x[1] = x[1][:-1]
    n.append(x)


with open("data/edges.csv") as f:    # construct edge list
    content = f.readlines()

e = []
for i in content:
    x = i.split(',')
    x[0] = int(x[0])
    x[1] = int(x[1])
    x[2] = int(x[2])
    x[3] = int(x[3])
    e.append(x)




# boring bit (actually its the fun bit I guess)

print(a, "to", b)
if c == "1":
    print("Using accessible route.")


g = graphDijkstra.Graph(n,e)
d = sortDijkstra.Dijkstra(g.getNodes())

try:
    print(d.solve(g, start, dest, acc))
except AssertionError as error:
    print(error)
