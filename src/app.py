from flask import Flask, flash, redirect, render_template, request, session, abort, url_for
from flask_wtf import FlaskForm
from wtforms import SelectField, BooleanField, SubmitField
from wtforms.validators import DataRequired, Email

from PIL import Image

import os
import time

from algo import graphDijkstra, sortDijkstra


# initialize graph lists from csv files

with open("data/nodes.csv") as f:    # construct node list
    content = f.readlines()

n = []
floor = {}  # dict of node heights
for i in content:
    x = i.split(',')
    x[0] = int(x[0])
    x[1] = x[1]#[:-1]
    x[2] = int(x[2])
    n.append(x[:-1]) # remove floor info from node

    floor[x[0]] = int(x[3]) # add floor info to dict


with open("data/edges.csv") as f:    # construct edge list
    content = f.readlines()

e = []
for i in content:
    x = i.split(',')
    x[0] = int(x[0])
    x[1] = int(x[1])
    x[2] = int(x[2])
    x[3] = int(x[3])
    e.append(x)

# generate graph
g = graphDijkstra.Graph(n,e)
d = sortDijkstra.Dijkstra(g.getNodes())




# flask code starts

app = Flask(__name__)
app.config['SECRET_KEY'] = 'skey'

class PathForm(FlaskForm):
    dest = SelectField('Destination:', coerce=int)
    access = BooleanField('Accessible?')
    submit = SubmitField('Submit')


# solve issues with cacheing
@app.after_request
def add_header(response):
    response.headers['X-UA-Compatible'] = 'IE=Edge,chrome=1'
    response.headers['Cache-Control'] = 'public, max-age=0'
    return response


# pages
@app.route("/", methods=['GET', 'POST'])
def index():
    # generate form for user input
    form = PathForm()
    form.dest.choices = []
    for i in n:
        if i[2]:
            form.dest.choices.append((i[0], i[1]))


    if form.validate_on_submit():
        return redirect(url_for('route', acc=form.access.data, dest=form.dest.data))

    # generate list of map images to pass to template, get top/bottom floors
    files = sorted(os.listdir("static/maps"))
    min = files[0].split('.')[0]
    max = files[-1].split('.')[0]

    images = ""
    for file in files:
        images = images + "{type: \'image\', url: \'static/maps/" + file + "\'},"
    images = images[:-1]


    return render_template(
        'home.html', form=form, images=images, min=min, max=max)



@app.route("/<int:acc>/<int:dest>")
def route(acc, dest):
    start = time.time()
    try:
        path = d.solve(g, 39, dest, acc)
        vectorList = []
        for i in range(len(path)-1): # works as graph is simple
            vectorList.append((path[i], path[i+1]))

        for mapName in os.listdir("static/maps"):
            # generate image based on vector list
            newMap = Image.open("static/maps/" + mapName)
            height = int(mapName.split('.')[0])

            for i in vectorList:
                # if vector is on current floor
                if floor[i[0]] == n or floor[i[1]] == height:
                    if os.path.isfile("static/vectors/" + str(i[0]) + "-" + str(i[1]) + ".png"):
                        line = Image.open("static/vectors/" +  str(i[0]) + "-" + str(i[1]) + ".png")
                        newMap.paste(line, (0,0), line)
                    else:
                        line = Image.open("static/vectors/" +  str(i[1]) + "-" + str(i[0]) + ".png")
                        newMap.paste(line, (0,0), line)

            newMap.save("static/mapPaths/" + mapName)

        # generate list of map images to pass to template, get top/bottom floors
        files = sorted(os.listdir("static/mapPaths"))
        min = files[0].split('.')[0]
        max = files[-1].split('.')[0]

        images = ""
        for file in files:
            images = images + "{type: \'image\', url: \'../static/mapPaths/" + file + "\'},"
        images = images[:-1]


        # generate form for user input
        form = PathForm()
        form.dest.choices = []
        for i in n:
            if i[2]:
                form.dest.choices.append((i[0], i[1]))

        end = time.time()
        print(end-start)
        return render_template(
            'home.html', form=form, images=images, min=min, max=max)


    except AssertionError as error:
        #just return normal home page but say theres an error
        return(str(error))


if __name__ == "__main__":
    app.run()
